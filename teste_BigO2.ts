namespace teste_BigO {

    let numero1, numero2, numero3: number;
    numero1 = 4;
    numero2 = 5;
    numero3 = 6;

    console.log(`${numero1}, ${numero1}`);
    console.log(`${numero1}, ${numero2}`);
    console.log(`${numero1}, ${numero3}`);

    console.log(`${numero2}, ${numero1}`);
    console.log(`${numero2}, ${numero2}`);
    console.log(`${numero2}, ${numero3}`);

    console.log(`${numero3}, ${numero1}`);
    console.log(`${numero3}, ${numero2}`);
    console.log(`${numero3}, ${numero3}`);
}