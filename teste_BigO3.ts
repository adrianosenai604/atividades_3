namespace teste_BigO {
    //utilização de um array para armazenamento de valores
    const numeros = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
    const numero_A_Encontrar = 7; // número a ser encontrado no array

    //armazenamento das posições do array
    let esquerda = 0;
    let direita = numeros.length - 1;

    while (esquerda <= direita) {
      //análise do array para encontrar o número desejado

      const metade = Math.floor((esquerda + direita) / 2); /* cálculo para definir o ponto de partida para a verificação*/

      if (numeros[metade] === numero_A_Encontrar) {
        //se o número for encontrado é mostrado a posição em que ele está
        console.log(`A posição do número ${numero_A_Encontrar} é a ${metade}!`);
        break; // finaliza a busca
      } else if (numeros[metade] < numero_A_Encontrar) {
        /*se o número em análise for menor que o número procurado, o controle da esquerda é alterado*/
        esquerda = metade + 1;
      } else {
        /*se o número desejado não foi encontrado e o número em análise for maior que o número a ser encontrado, o controle da direita é atualizado*/
        direita = metade - 1;
      }
    }//fim do while  
}//fim do namespace
